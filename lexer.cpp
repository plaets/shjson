#pragma once
#include <memory>
#include <string>
#include <iostream>

namespace shjson::Lexer {
    struct Token;
    struct ArrayOpen;
    struct ArrayClose;
    struct StructOpen;
    struct StructClose;
    struct End;
    struct Comma;
    struct Colon;
    struct Null;
    struct Bool;
    struct String;
    struct Number;

    class TokenVisitor {
    public:
        virtual void visit(ArrayOpen&) = 0;
        virtual void visit(ArrayClose&) = 0;
        virtual void visit(StructOpen&) = 0;
        virtual void visit(StructClose&) = 0;
        virtual void visit(End&) = 0;
        virtual void visit(Comma&) = 0;
        virtual void visit(Colon&) = 0;
        virtual void visit(Null&) = 0;
        virtual void visit(Bool&) = 0;
        virtual void visit(String& tok) = 0;
        virtual void visit(Number& tok) = 0;
    };

    struct Token{
        virtual ~Token(){}
        virtual void accept(TokenVisitor& v) = 0;
    };

    struct ArrayOpen: public Token{
        virtual void accept(TokenVisitor& v) { v.visit(*this); }
    };

    struct ArrayClose: public Token{
        virtual void accept(TokenVisitor& v) { v.visit(*this); }
    };

    struct StructOpen: public Token{
        virtual void accept(TokenVisitor& v) { v.visit(*this); }
    };

    struct StructClose: public Token{
        virtual void accept(TokenVisitor& v) { v.visit(*this); }
    };

    struct End: public Token{
        virtual void accept(TokenVisitor& v) { v.visit(*this); }
    };

    struct Comma: public Token{
        virtual void accept(TokenVisitor& v) { v.visit(*this); }
    };

    struct Colon: public Token{
        virtual void accept(TokenVisitor& v) { v.visit(*this); }
    };

    struct Null: public Token{
        virtual void accept(TokenVisitor& v) { v.visit(*this); }
    };

    struct Bool: public Token{
        bool value;
        
        Bool(bool val): value{val} {}
        virtual void accept(TokenVisitor& v) { v.visit(*this); }
    };

    struct String: public Token{
        std::string value;

        String(std::string val): value{val} {}
        virtual ~String() {}
        virtual void accept(TokenVisitor& v) { v.visit(*this); }
    };

    struct Number: public Token{
        std::string value; //we will parse it later 

        Number(std::string val): value{val} {}
        virtual ~Number() {}
        virtual void accept(TokenVisitor& v) { v.visit(*this); }
    };

    class LexerException{
        std::string msg;
        size_t pos;

    public:
        LexerException(std::string what, size_t pos){
            this->msg = what;
            this->pos = pos;
        }

        auto what(){ return msg; }
        auto position(){ return pos; }
    }; 

    bool isNumber(char c){
        return c >= 48 && c <= 57;
    }

    bool isWhitespace(char c){
        return c == ' ' || c == '\n' || c == '\t' || c == '\r';
    }

    class Lexer {
        size_t pos = -1;
        std::string& json;

        std::unique_ptr<String> ParseString(){
            std::string string;
            size_t i;

            bool escape = false; //i tried my best to avoid this
            for(i = pos+1; i != json.length(); i++) {
                if(json[i] == '\\') {
                    escape = true;
                    string += json[i];
                    continue;
                }

                if(escape || json[i] != '"') {
                    string += json[i];
                    escape = false;
                } else {
                    break;
                }
            }

            if(json[i] != '\"')
                throw LexerException("String not terminated", pos);

            pos = i;
            return std::make_unique<String>(string);
        }

        std::unique_ptr<Number> ParseNumber(){
            size_t i;
            for(i = pos; i != json.length(); i++)
                if(isNumber(json[i]) || json[i] == '.' || json[i] == 'e' || json[i] == 'E') continue;
                else if(i == pos && (json[i] == '-' || json[i] == '+')) continue;
                else break;

            auto oldPos = pos;
            pos = i-1;
            return std::make_unique<Number>(json.substr(oldPos, i-oldPos));
        }

        std::unique_ptr<Null> ParseNull(){
            if((json.length()-1)-pos >= 3 && json.find("null", pos) == pos){
                pos+=3;
                return (std::make_unique<Null>());
            }else{
                throw LexerException("Unknown token", pos);
            }
        }

        std::unique_ptr<Bool> ParseBool(){
            if((json.length()-1)-pos >= 3 && json.find("true", pos) == pos){
                pos+=3;
                return (std::make_unique<Bool>(true));
            }else if((json.length()-1)-pos >= 4 && json.find("false", pos) == pos){
                pos+=4;
                return (std::make_unique<Bool>(false));
            }else{
                throw LexerException("Unknown token", pos);
            }
        }

    public:
        Lexer(std::string& jsonStr): json{jsonStr}{}

        std::unique_ptr<Token> lex(){
            while(true){
                pos++;
                if(pos >= json.size()-1) return std::make_unique<End>();
                else if(json[pos] == '[') return (std::make_unique<ArrayOpen>());
                else if(json[pos] == ']') return (std::make_unique<ArrayClose>());
                else if(json[pos] == '{') return (std::make_unique<StructOpen>());
                else if(json[pos] == '}') return (std::make_unique<StructClose>());
                else if(json[pos] == ',') return (std::make_unique<Comma>());
                else if(json[pos] == ':') return (std::make_unique<Colon>());
                else if(json[pos] == 'n') return ParseNull();
                else if(json[pos] == 't' || json[pos] == 'f') return ParseBool();
                else if(isNumber(json[pos]) || json[pos] == '-' || json[pos] == '+' || json[pos] == '.') return ParseNumber();
                else if(json[pos] == '"') return ParseString();
                else if(!isWhitespace(json[pos])) throw LexerException("Unknown token", pos);
            }
        }
    };

    class TokenPrinter: public TokenVisitor {
        bool end = false;

    public:
        TokenPrinter(Lexer& lexer){ while(!end) lexer.lex()->accept(*this); }

        virtual void visit(ArrayOpen&) override { std::cout<<"[ "; }
        virtual void visit(ArrayClose&) override { std::cout<<"] "; }
        virtual void visit(StructOpen&) override { std::cout<<"{ "; }
        virtual void visit(StructClose&) override { std::cout<<"} "; }
        virtual void visit(Comma&) override { std::cout<<", "; }
        virtual void visit(Colon&) override { std::cout<<": "; }
        virtual void visit(Null&) override { std::cout<<"null "; }
        virtual void visit(Bool& tok) override { std::cout<<(tok.value ? "true" : "false")<<" "; }
        virtual void visit(String& tok) override { std::cout<<"\""<<tok.value<<"\" "; }
        virtual void visit(Number& tok) override { std::cout<<tok.value<<" "; }
        virtual void visit(End&) override { end = true; }
    };
}
