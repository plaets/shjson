#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <typeinfo>
#include <type_traits>
#include "lexer.cpp"
#include "parser.cpp"

int main(int argc, char** argv){
    if(argc > 1) {
        std::ifstream file(argv[1]); //https://stackoverflow.com/a/2602258/9572217
        std::stringstream buffer;
        buffer<<file.rdbuf();
        std::string str = buffer.str();

        try{
            auto lexer = shjson::Lexer::Lexer(str);
            //Lexer::TokenPrinter printer(lexer);
            shjson::Parser::Parser p;
            auto el = p.parse(lexer);
            if(el) std::cout<<shjson::Parser::stringifyElement(*el)<<std::endl;
        }catch(shjson::Lexer::LexerException& e){
            std::cerr<<e.what()<<" on pos: "<<e.position()<<" character: "<<str[e.position()]<<std::endl;
        }
    } else {
        std::cout<<"Usage: "<<argv[0]<<" file"<<std::endl;
    }
}
