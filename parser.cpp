#pragma once
#include <variant>
#include <map>
#include <memory>
#include <string>
#include <vector>
#include <stack>
#include "lexer.cpp"

namespace shjson::Parser {
    enum class Type {
        Null,
        Bool,
        Number,
        String,
        Array,
        Struct
    };

    struct Element;

    struct Number {
        std::string strValue;

        Number(std::string& val): strValue{val} {
        }
    };

    using String = std::string;
    using Array = std::vector<std::unique_ptr<Element>>;
    using Struct = std::map<std::string, std::unique_ptr<Element>>;

    using NumberPtr = std::unique_ptr<Number>;
    using StringPtr = std::unique_ptr<String>;
    using ArrayPtr = std::unique_ptr<Array>;
    using StructPtr = std::unique_ptr<Struct>;

    struct Element {
        Type type;
        std::variant<
            bool,
            NumberPtr,
            StringPtr,
            ArrayPtr,
            StructPtr
        > value;
    };

    enum class ExpectedElement {
        Comma,
        Colon,
        Key,
        Value,
        Nothing
    };

    std::string TypeToString(Type type){
        if(type == Type::Null) return "null";
        else if(type == Type::Bool) return "bool";
        else if(type == Type::Number) return "number";
        else if(type == Type::String) return "string";
        else if(type == Type::Array) return "array";
        else if(type == Type::Struct) return "struct";
        else return "unknown";
    }

    std::string ExpectedElementToString(ExpectedElement expected){
        if(expected == ExpectedElement::Value) return "value";
        else if(expected == ExpectedElement::Colon) return "colon";
        else if(expected == ExpectedElement::Comma) return "comma";
        else if(expected == ExpectedElement::Key) return "key";
        else if(expected == ExpectedElement::Nothing) return "nothing";
        else return "unknown";
    }

    //auto createElement(Type type){
    //}
     
    class Parser: Lexer::TokenVisitor {
    private:
        std::unique_ptr<Element> element;
        std::stack<Element*> scopes;

        std::unique_ptr<std::string> currentKey;
        ExpectedElement expected = ExpectedElement::Value;
        bool end = false;

    public:
        std::unique_ptr<Element> parse(Lexer::Lexer& lexer){
            while(!end) lexer.lex()->accept(*this);
            if(scopes.size() != 0) throw std::runtime_error("Array or struct is not closed");
            return std::move(element);
        }

        template<typename T>
        auto CreateElement(Type type){
            auto el = std::make_unique<Element>();
            el->type = type;
            el->value = std::make_unique<T>();
            return std::move(el);
        }

        template<typename T>
        void AddToCurrentArray(T el){
            std::get<ArrayPtr>(scopes.top()->value)->push_back(std::move(el));
        }

        template<typename T>
        void AddToCurrentStruct(T el){
            (*std::get<StructPtr>(scopes.top()->value))[*currentKey] = std::move(el);
            currentKey = nullptr;
        }

        template<typename T, typename E=std::string>
        void add(T el, Type type, E* val=nullptr){
            if(!element){
                element = std::move(el);
            }else{
                if(scopes.size() > 0 && scopes.top()->type == Type::Array){
                    AddToCurrentArray(std::move(el));
                }else if(scopes.size() > 0 && scopes.top()->type == Type::Struct){
                    if(currentKey) AddToCurrentStruct(std::move(el));
                    else if(val) currentKey = std::make_unique<E>(*val);
                    else throw std::runtime_error(TypeToString(type) + "s cannot be struct keys");
                }else{
                    throw std::runtime_error("Unexpected" + TypeToString(type));
                }
            }
        }

        template<typename T>
        void addScoped(Type type){
            if(expected == ExpectedElement::Value){
                auto el = CreateElement<T>(type);
                auto ptr = el.get();
                expected = ExpectedElement::Value;
                add(std::move(el), type); 
                scopes.push(ptr);
            }else{
                throw std::runtime_error("Unexpected " + TypeToString(type));
            }
        }

        template<typename T, typename E>
        void addScalar(Type type, E val){
            if(expected == ExpectedElement::Value){
                auto el = std::make_unique<Element>();
                el->type = type; 
                el->value = std::move(val);
                if(std::is_same<T, String>::value)
                    add(std::move(el), type, std::get<StringPtr>(el->value).get()); 
                else
                    add(std::move(el), type); 
                expected = ExpectedElement::Comma;
            }else{
                throw std::runtime_error("Unexprected " + TypeToString(type) + " value"); 
            }
        }

        template<typename T>
        void close(Type type){
            if(scopes.size() > 0 && scopes.top()->type == type){
                bool waitingForComma = (expected == ExpectedElement::Comma && 
                        std::get<std::unique_ptr<T>>(scopes.top()->value)->size() > 0);
                bool waitingForValue = (std::get<std::unique_ptr<T>>(scopes.top()->value)->size() == 0 && 
                        expected == ExpectedElement::Value);

                if(waitingForComma || waitingForValue){
                    scopes.pop();
                    if(scopes.size() == 0) expected = ExpectedElement::Nothing;
                    else expected = ExpectedElement::Comma;
                    return;
                }
            }

            throw std::runtime_error("Unexpected " + TypeToString(type) + " close");
        }

        void visit(Lexer::ArrayOpen&){ addScoped<Array>(Type::Array); }
        void visit(Lexer::ArrayClose&){ close<Array>(Type::Array); }
        void visit(Lexer::StructOpen&){ addScoped<Struct>(Type::Struct); }
        void visit(Lexer::StructClose&){ close<Struct>(Type::Struct); }

        void visit(Lexer::Comma&){
            if(expected != ExpectedElement::Comma)
                throw std::runtime_error("Unexpected comma");

            expected = ExpectedElement::Value;
        }

        void visit(Lexer::Colon&){
            if(!(currentKey && scopes.top()->type == Type::Struct))
                throw std::runtime_error("Unexpected colon");

            expected = ExpectedElement::Value;
        }

        void visit(Lexer::Null&){
            if(expected == ExpectedElement::Value){
                auto el = std::make_unique<Element>();
                el->type = Type::Null; 
                add(std::move(el), Type::Null); 
                expected = ExpectedElement::Comma;
            }else{
                throw std::runtime_error("Unexprected null value"); 
            }
        }

        void visit(Lexer::Bool& val){ addScalar<bool>(Type::Bool, val.value); }
        void visit(Lexer::String& val){ addScalar<String>(Type::String, std::make_unique<String>(val.value)); }
        void visit(Lexer::Number& val){ addScalar<Number>(Type::Number, std::make_unique<Number>(val.value)); }
        void visit(Lexer::End&){ end = true; }
    };   

    std::string repeat(std::string str, int num){
        std::string r = "";
        for(auto i = 0; i != num; i++) r += str;
        return r;
    }

    std::string stringifyElement(const shjson::Parser::Element&);

    std::string stringifyArray(const shjson::Parser::Element& el){
        std::stringstream ss;
        auto& n = *std::get<shjson::Parser::ArrayPtr>(el.value);
        for(size_t i = 0; i != n.size(); i++){
            ss<<stringifyElement(*n[i]);
            if(i != n.size()-1) ss<<",";
        }

        return ss.str();
    }


    std::string stringifyStruct(const shjson::Parser::Element& el){
        std::stringstream ss;
        auto& m = *std::get<shjson::Parser::StructPtr>(el.value);
        for(auto it = m.begin(); it!=m.end(); ++it){
            ss<<"\""<<it->first<<"\": ";
            ss<<stringifyElement(*it->second);
            if(std::next(it) != m.end()) ss<<",";
        }

        return ss.str();
    }

    std::string stringifyElement(const shjson::Parser::Element& el){ //*le json beautifier*
        std::stringstream ss;

        if(el.type == shjson::Parser::Type::Array)
            ss<<"["<<stringifyArray(el)<<"]";
        else if(el.type == shjson::Parser::Type::Struct)
           ss<<"{"<<stringifyStruct(el)<<"}";
        else if(el.type == shjson::Parser::Type::Number)
           ss<<std::get<shjson::Parser::NumberPtr>(el.value)->strValue;
        else if(el.type == shjson::Parser::Type::Bool)
           ss<<(std::get<bool>(el.value) ? "true" : "false");
        else if(el.type == shjson::Parser::Type::String)
            ss<<"\""<<*std::get<shjson::Parser::StringPtr>(el.value)<<"\"";
        else if(el.type == shjson::Parser::Type::Null)
            ss<<"null";
        else
            ss<<"unknown type";

        return ss.str();
    }
}
