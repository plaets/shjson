# JSON Parser 

JSON Parser made using a weird mix of std::variant and OOP visitor pattern. 
Lots of redundant code right now.

## How to run

`g++ --std=c++17 shjson.cpp -O3 -o shjson`

After compilation, shjson will be the resulting executable file.
The executable will parse any file named `test.json` and print it in a weird, made up text format.
A long-term target is to turn this into another json library, we will see how it goes... 
